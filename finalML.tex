\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{anysize}
\marginsize{4cm}{3cm}{3cm}{3cm}
\usepackage{hyperref}
\usepackage{natbib}
\def\bibname{Referencias}%
% Title Page
\title{Clasificación de candidatos a transitorios en imágenes astronómicas mediante Machine Learning}
\author{Bruno Sánchez}


\begin{document}
\maketitle

\begin{abstract}
En el presente reporte se presenta el problema de la clasificación binaria entre
objetos reales y espúreos (\textit{bogus}) provenientes de imágenes astronómicas.
Se simularon ejmplos de ambas clases de objetos, generando una muestra de datos
de volumen considerable. Se extrajeron \textit{features} de cada imagen, creando
un \textit{input} ideal para procesar con técnicas de Aprendizaje Automático.
Se realizo una selección de los features y se llevó a cabo una implementación 
de diversos algoritmos de Aprendizaje Automático enlatados en la distribución Weka.
\end{abstract}

\section{Introducción}
En el área de la astronomía denominada ``variabilidad'' se está gestando un cambio notable
gracias a la tecnología de la informática. En el presente diversos instrumentos astronómicos
están midiendo los cielos constantemente, a la espera de eventos no antes vistos. 
%
Un caso de estos eventos son los llamados ``transitorios'' los cuales son cambios repentinos
y momentáneos de la luminosidad de ciertos objetos de esta u otras galaxias. Con frecuencia 
estos sucesos corresponden a estallidos de Supernovas, los cuales son consecuencia 
de la muerte de las estrellas más masivas que existen.
%
Detectar estos raros sucesos es importante para la investigación en el origen y la vida de las
estrellas y las galaxias en general.\\
%
Para encontrar estos objetos se realizan ``relevamientos'' del cielo (\cite{djorgovski_sky_2012}),
es decir telescopios que observan las mismas áreas de la esfera celeste noche tras noche, generando una historia
de imágenes -o algo parecido a una ``pelicula''- de los objetos astronómicos observados (\cite{2012PASP..124.1175B}).\\
%
Como los transitorios ocurren esporádicamente, y no vuelven a repetirse en un mismo lugar, es
díficil hallar Supernovas en éstas ``películas'' recurriendo a la inspección manual de un 
experto. Esta tarea -en otras épocas la técnica más común- ya es imposible de llevar a cabo 
por humanos, debido a los grandes volúmenes de datos que los relevamientos antes mencionados
generan noche a noche (\cite{cavuoti_data-rich_2013, ivezic_lsst:_2008}), es por esto que cada 
vez se utilizan más los llamados Algoritmos de aprendizaje automático: 
\cite{2013MNRAS.435.1047B, 2011ApJ...733...10R, 2012amld.book...89B, brink_using_2012}. 
%
\section{Breve introducción a los algoritmos de Aprendizaje Automático}
En las ciencias de la computación existe el área denominada \textit{Machine Learning} o
bien ``Aprendizaje automático'' en castellano. Esta rama de la inteligencia artificial
se enfoca en utilizar, principalmente, técnicas estadísticas para lograr que una computadora
\textit{aprenda}, (\cite{mlearn}). 
%
Esto es, lograr que el rendimiento, de un programa de computadora, al realizar una tarea
mejore con la experiencia (es decir número de realizaciones de esta misma tarea).\\
%
Estos algoritmos se aplican a diversos problemas, entre los cuales se halla el problema 
de clasificación -que es el que atacaremos en este reporte-. El problema de clasificación
suele abordarse utilizando una \textit{clase objetivo} y diversos \textit{features} asociados
a un conjunto de datos, y donde el algoritmo debe crear un modelo sobre estos elementos.
%
El conjunto de datos se lo denomina de \textit{entrenamiento} ya que sirve de ejemplos para 
que el algoritmo construya el modelo, los \textit{features} son la 
información que será utilizada por el modelo para clasificar nuevas mediciones, y la \textit{clase objetivo}
es el dato que deseamos que nuestro modelo prediga. \\
%
De esta forma existen varios algoritmos que, según sean las particularidades del problema y de los datos
de entrenamiento, pueden aplicarse y así mediante ejemplos clasificar nuevas instancias.

\section{El problema de identificación Bogus-Real}
Para llevar a cabo búsquedas de transitorios en el cielo se utiliza una técnica nueva 
que involucra el procesado digital de cada nueva imagen obtenida y su posterior 
contraposición con los datos previos de la misma zona del cielo observada. Este tipo de técnicas
se la denomina \textbf{DIA} (\textit{Differential Image Photometry}) y posee el siguiente 
procedimiento:
\begin{enumerate}
 \item Se genera una imagen \textit{maestra} (o ``master frame'') de una zona del cielo. 
 La misma debe poseer la mejor resolución disponible, y la mayor profundidad posible.
 \item Al obtener mediciones nuevas de esta porción del cielo se procede a la calibración entre las
 nuevas imágenes y la \textit{master frame}. Esta calibración es tanto astrométrica como fotométrica.
 \item Se calcula la diferencia píxel a píxel entre las nuevas imágenes ya calibradas y el \textit{master frame}.
 \item Se procede a identificar nuevas fuentes en el residuo del cálculo anterior. 
\end{enumerate}

Este método utiliza la premisa de que es probable que si no ha habido variabilidad en la zona observada
la imagen residual posea una distribución gaussiana (ruido) en el valor de los pixeles.
Si esta distribución posee una cola entonces la diferencia entre la imagen nueva y la maestra
posea evidencia de un evento transitorio.\\
%
Esta técnica funciona muy bien ya que encuentra la gran mayoría de los eventos transitorios 
en el cielo con altas tasas de completitud. Pero su problema mayor es que los candidatos que
genera sufren de una importante contaminación. Esto es debido a que las calibraciones involucradas 
en el proceso son imperfectas y generan errores en los pixeles residuales que se apartan de la
distribución gaussiana propia del ruido.
%
Estos falsos candidatos se los denomina \textit{bogus}. 
En la Fig. \ref{fig:bogus} se muestran ejemplos de los \textit{bogus} simulados.
Estos objetos son producto del ruido estocástico, la forma de simularlos es incluyendo
ruido en el procesado de las imágenes y al identificar sobre la imagen residual las fuentes
astronómicas se extraen ``parches'' -pequeñas imágenes- de estos candidatos.

\begin{figure}
 \centering
 \includegraphics[scale=1]{../../../codigos/scipycodes/image-simulation-and-analysis/bogus/obj_002.png}
 \includegraphics[scale=1]{../../../codigos/scipycodes/image-simulation-and-analysis/bogus/obj_011.png}
 \includegraphics[scale=1]{../../../codigos/scipycodes/image-simulation-and-analysis/bogus/obj_013.png}
 \includegraphics[scale=1]{../../../codigos/scipycodes/image-simulation-and-analysis/bogus/obj_018.png}
 % obj_002.png: 64x64 pixel, 72dpi, 2.26x2.26 cm, bb=0 0 64 64
 \caption{Ejemplos de parches de bogus simulados.}
 \label{fig:bogus}
\end{figure}

Distinta es la simulación de objetos reales, ya que si bien incluyen ruido en la resta de imágenes, se
simula un objeto tipo estrella (se utiliza una PSF) y solo se extraen los parches sobre los objetos que
inyectamos. En la Fig. \ref{fig:reals} se muestran ejemplos de nuestros objetos clase real simulados.

\begin{figure}
 \centering
 \includegraphics[scale=1]{../../../codigos/scipycodes/image-simulation-and-analysis/real/robj_0025.png}
 \includegraphics[scale=1]{../../../codigos/scipycodes/image-simulation-and-analysis/real/robj_0041.png}
 \includegraphics[scale=1]{../../../codigos/scipycodes/image-simulation-and-analysis/real/robj_0040.png}
 \includegraphics[scale=1]{../../../codigos/scipycodes/image-simulation-and-analysis/real/robj_0115.png}
 % obj_002.png: 64x64 pixel, 72dpi, 2.26x2.26 cm, bb=0 0 64 64
 \caption{Ejemplos de parches de objetos reales simulados.}
 \label{fig:reals}
\end{figure}


\section{Simulación de datos y extracción de features}
De esta forma simulamos 9202 \textit{bogus} y 9120 \textit{reals}. Esta muestra se generó mediante 
herramientas de astronomía observacional provenientes del paquete IRAF\footnote{\url{http://iraf.noao.edu/}}, el
cual está embebido en el modulo PyRAF\footnote{\url{http://www.stsci.edu/institute/software_hardware/pyraf/}}\footnote{PyRAF is a product of the Space Telescope Science Institute,
which is operated by AURA for NASA} de Python. Además se utilizó software de Astromatic, a saber:
SExtractor\footnote{\url{http://www.astromatic.net/software/sextractor}} \citep{2005astro.ph.12139H}, 
SWarp\footnote{\url{http://www.astromatic.net/software/swarp}} y 
STIFF\footnote{\url{http://www.astromatic.net/software/stiff}}.\\ 

Para extraer features utilizables para algoritmos de Aprendizaje Automático se utilizó una herramienta
de clasificación de imágenes desarrollada para biologia llamada \textbf{wnd-charm}\footnote{\url{http://www.scfbm.org/content/3/1/13}, 
link a Github:\url{https://github.com/wnd-charm/wnd-charm}} \cite{shamir_wndchrm_2008}. 
Esta herramienta aplica una batería de transformaciónes de imágenes (por ejemplo Fourier, Wavelet) y calcula sobre estas estadísticos, 
como ser coeficientes de expansión en polinomios de Zernike, Chebyshev, Haralicke, estadísticos de Radon, etc.

En total los features extraídos son un total de 1060.\\ 

Para trabajar con Machine Learning se escribieron los datos en formato \verb .arff  (provenientes 
de \textit{attribute relation format file}), el cual es el formato de transferencia de datos
utilizado por Weka.

Weka es una coleccion de herramientas de Machine Learning desarrolladas en java. Posee algoritmos de
clasificacion, de \textit{clustering} o ``agrupamiento'',
de regresión, de asociación y de \textit{feature selection}.
Mediante esta herramienta se trabajó sobre los datos simulados
y se evaluaron los rendimientos de varios algoritmos de clasificación.

\section{Algoritmos aplicados}
Durante esta sección mostraremos los algoritmos aplicados a la muestra y 
mencionaremos sus principales características.

\subsection{Algoritmo de \textit{feature selection}}
Para seleccionar \textit{features} se utilizan diversos métodos estadísticos.
El objetivo principal es reducir el tamaño del conjunto de \textit{features} al más pequeño, 
conservando la mayor cantidad de información posible.\\
%
Uno de los métodos consiste en medir la correlación entre cada \textit{feature} y el 
valor de la clase objetivo, y la correlación entre los otros \textit{features}. 
Los \textit{features} que maximizen el primero y minimizen el segundo serán los que se presumen
óptimos.\\
%
Otro método más intensivo es llevar a cabo un análisis de componentes principales (PCA) 
sobre los \textit{features} y aplicar la transformación ortogonal a los datos, de forma tal
que los nuevos \textit{features} representen las proyecciones de los anteriores sobre estas componentes principales.

\subsection{Algoritmo de Naive Bayes}
El algoritmo de Naive Bayes utiliza el teorema de Bayes (ecuación \ref{eqn:bayes}), el cual indica que las probabilidades 
condicionales de dos fenómenos aleatorios A y B se relacionan simétricamente.
\begin{equation}
\label{eqn:bayes}
 P(A|B) P(B) = P(B|A)P(A)
\end{equation}
De esta forma se puede estimar la probabilidad de que un cierto modelo describa un conjunto de datos.
Sea $y$ la clase objetivo, con $y_k$ una clase en particular, y $\vec{f}$ el vector que contiene los \textit{features},
entonces podemos, utilizando lo anterior, expresar la probabilidad de que $y_k$ sea la clase correcta dados $\vec{f}$
tal como en la ecuación \ref{eqn:bayes2}.
\begin{equation}
\label{eqn:bayes2}
 P(y=y_k|\vec{f}) = \frac{P(\vec{f}|y=y_k)P(y=y_k)}{P(\vec{f})}
\end{equation}
%
El algoritmo entonces evalúa para cada clase $y_k$ la probabilidad dados los features medidos en $\vec{f}$, 
y luego debe decidir con cual quedarse. 
%
Dos formas de elegir son: utilizando un \textit{Maximum a posteriori} o MAP, 
el cual elige la clase que maximiza $P(\vec{f}|y=y_k)P(y=y_k)$; y una segunda forma
es utilizando el \textit{Maximum Likelihood} o ML, que maximiza $P(\vec{f}|y=y_k)$.
%
\subsection{Regresion Logística}
Los clasificadores que utilizan regresi\'on log\'{i}stica implementan un modelo de regresi\'on lineal 
de la forma dada en la ecuación \ref{eqn:rlogi}
\begin{equation}
\label{eqn:rlogi}
 logi(p_i) = \ln(\frac{p_i}{1-p_i}) = \alpha + \beta \times x_i
\end{equation}

Esto es lo mismo que decir que mapeamos las tasas de \'exito $p_i/1-p_i$ (pertenecientes al intervalo real entre 0 y 1),
al eje real $(-\inf, +\inf)$ mediante una funci\'on log\'{i}stica dada por la ecuación \ref{eqn:logi}.
\begin{equation}
\label{eqn:logi}
 logi(x) = \ln(\frac{x}{1-x}) 
\end{equation}

Así mediante un ajuste lineal podemos asignar probabilidades a cada valor de cada \textit{feature} 
y luego elegir la clase que las maximiza.

\subsection{Random Forest}
Este método se aplica cuando hay un razonable número de \textit{features} que son susceptibles de 
ser discretizables. El procedimiento consta de generar muchos árboles de decisión, donde cada 
árbol utiliza un subconjunto aleatorio de \textit{features}, que debe ser de tamaño mucho menor al 
conjunto total. 
%
Una vez entrenado el modelo, para clasificar nuevas instancias
cada árbol emite un voto según sea la predicción a la que arribó, y luego se utiliza
el voto más repetido para emitir la predicción final.

\section{Resultados}
\subsection{Resultados de la \textit{Feature Selection}}
Se aplicaron dos técnicas: PCA, y \textbf{CfsSubsetEval} de Weka \cite{Hall1998} que implementa
selecciones mediante correlaciones.

Para PCA el resultado arrojó 310 componentes principales, y se transformaron los datos.

La segunda técnica encontró que los mejores features eran 39, luego se generó una submuestra con los mismos.

\subsection{Resultados muestra completa}
\subsubsection{Naive Bayes}
Se entrenó un clasificador de Naive Bayes (NB), el incluido en Weka.
Se incluye a continuación parte de la salida del \textit{log} de resultados.
Se muestran además los resultados de la evaluación de errores mediante una
\textit{cross-correlation} en 10 hojas.

\begin{verbatim}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
=== Run information ===

Scheme:weka.classifiers.bayes.NaiveBayes 
Relation:     realbogus-weka.filters.unsupervised.attribute.Remove-R1060
Instances:    18322
Attributes:   1060
[list of attributes omitted]
Test mode:10-fold cross-validation

=== Classifier model (full training set) ===
Naive Bayes Classifier

Time taken to build model: 10.75 seconds

=== Stratified cross-validation ===

=== Summary ===
Correctly Classified Instances       10103               55.1414 %
Incorrectly Classified Instances      8219               44.8586 %
Total Number of Instances            18322     

=== Detailed Accurac\bibliographystyle{aa}
y By Class ===
       TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
         0.971     0.872      0.529     0.971     0.685      0.554    bogus
         0.128     0.029      0.813     0.128     0.222      0.746    real
W. Avg.  0.551     0.452      0.67      0.551     0.454      0.65 

=== Confusion Matrix ===
   a    b   <-- classified as
 8932  270 |    a = bogus
 7949 1171 |    b = real
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{verbatim}
%Kappa statistic                          0.0994
%Mean absolute error                      0.4486
%Root mean squared error                  0.6696
%Relative absolute error                 89.7269 %
%Root relative squared error            133.9164 %

Se puede ver que se obtuvieron resultados pobres, ya que casi todas las instancias fueron 
clasificadas como bogus, lo que puede notarse claramente en el porcentaje de 
instancias clasificadas correctamente que ronda el 55\%.

Se puede notar que la calidad del clasificador NB para la muestra completa es baja, 
y otras técnicas podrían mejorar estos resultados facilmente.

%
%\subsubsection{Resultados de Logistic Regresion}
%El entrenamiento de Regresión Logística no pudo ser implementado, debido al tiempo de cómputo que exigía
%en una sola CPU. Por lo tanto se sugiere correr en algún sistema distribuido.

\subsubsection{Random Forest}
Se entrenó un clasificador tipo Random Forest sobre la muestra completa, 
y se aplicó la técnica de \textit{Cross-correlation} con 10 hojas para evaluar
el rendimiento.
Se muestra a continuación la salida en pantalla de Weka.
\begin{verbatim}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 === Run information ===

Scheme:weka.classifiers.trees.RandomForest -I 100 -K 0 -S 1
Relation:     realbogus-weka.filters.unsupervised.attribute.Remove-R1060
Instances:    18322
Attributes:   1060
[list of attributes omitted]
Test mode:10-fold cross-validation

=== Classifier model (full training set) ===
Random forest of 100 trees, 
each constructed while considering 11 random features.
Out of bag error: 0.1159

Time taken to build model: 62.87 seconds

=== Stratified cross-validation ===

=== Summary ===
Correctly Classified Instances       16277               88.8386 %
Incorrectly Classified Instances      2045               11.1614 %
Total Number of Instances            18322     

=== Detailed Accuracy By Class ===
       TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
         0.902     0.125      0.879     0.902     0.89       0.958    bogus
         0.875     0.098      0.898     0.875     0.886      0.958    real
W. Avg.  0.888     0.112      0.889     0.888     0.888      0.958

=== Confusion Matrix ===
    a    b   <-- classified as
 8300  902 |    a = bogus
 1143 7977 |    b = real
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{verbatim}
%Kappa statistic                          0.7767
%Mean absolute error                      0.2409
%Root mean squared error                  0.3072
%Relative absolute error                 48.1881 %
%Root relative squared error             61.4431 %

Vemos que el clasificador obtuvo rendimientos similares para ambas clases, mostrando
una \'area bajo la curva ROC de 0.958 en ambos casos. Se observó que este clasificador 
obtuvo un rendimiento muy bueno, considerando que se aplicó sobre la muestra completa de 
1060 \textit{features}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Resultados para la muestra transformada mediante PCA}
Esta muestra se conforma de 310 \textit{features} computados mediante combinaciones lineales de los 1060 
originales. 
Esta muestra es por lo tanto más pequeña que la anterior, y una vez transformados los datos
entrenar y evaluar modelos es mucho menos intensivo computacionalmente.

\subsubsection{Naive Bayes}
Se entrenó un clasificador tipo Naive Bayes con la submuestra transformada mediante PCA.
Se incluye a continuación parte de la salida del \textit{log} de resultados.
Se muestran además los resultados de la evaluación de errores mediante una
\textit{cross-correlation} en 10 hojas.

\begin{verbatim}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
=== Run information ===

Scheme:weka.classifiers.bayes.NaiveBayes 
Relation:     AT: realbogus-weka.filters.unsupervised.attribute...
Instances:    18322
Attributes:   311
[list of attributes omitted]
Test mode:10-fold cross-validation

=== Classifier model (full training set) ===
Naive Bayes Classifier

Time taken to build model: 3.93 seconds

=== Stratified cross-validation ===

=== Summary ===
Correctly Classified Instances       10392               56.7187 %
Incorrectly Classified Instances      7930               43.2813 %
Total Number of Instances            18322     

=== Detailed Accuracy By Class ===
      TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
        0.911     0.78       0.541     0.911     0.679      0.752    bogus
        0.22      0.089      0.71      0.22      0.336      0.752    real
W. Avg. 0.567     0.436      0.625     0.567     0.508      0.752

=== Confusion Matrix ===
    a    b   <-- classified as
 8382  820 |    a = bogus
 7110 2010 |    b = real
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{verbatim}
%Kappa statistic                          0.1317
%Mean absolute error                      0.4242
%Root mean squared error                  0.6166
%Relative absolute error                 84.8475 %
%Root relative squared error            123.3173 %

Se puede notar que el clasificador posee un sesgo hacia la clase \textbf{bogus}.
Esto puede pensarse de la siguiente forma: el clasificador elige la clase \textbf{bogus} en el 80\% de los casos 
independientemente de que clase realmente sea el objeto.


\subsubsection{Regresión Logística}
Se entrenó un clasificador que emplea Regresión Logítica sobre la muestra obtenida
mediante análisis de PCA.
Se empleó una \textit{cross-correlation} en 10 hojas para determinar el rendimiento
del clasificador.
Se incluye nuevamente a continación la salida de pantalla de resultados de Weka.

\begin{verbatim}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
=== Run information ===
Scheme:weka.classifiers.functions.Logistic -R 1.0E-8 -M -1
Relation:     AT: realbogus-weka.filters.unsupervised.attribute...
Instances:    18322
Attributes:   311
[list of attributes omitted]
Test mode:10-fold cross-validation

=== Classifier model (full training set) ===
Logistic Regression with ridge parameter of 1.0E-8
Time taken to build model: 95.41 seconds

=== Stratified cross-validation ===

=== Summary ===
Correctly Classified Instances       16134               88.0581 %
Incorrectly Classified Instances      2188               11.9419 %
Total Number of Instances            18322     

=== Detailed Accuracy By Class ===
       TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
         0.884     0.123      0.879     0.884     0.881      0.942    bogus
         0.877     0.116      0.882     0.877     0.88       0.942    real
W. Avg.  0.881     0.119      0.881     0.881     0.881      0.942

=== Confusion Matrix ===
    a    b   <-- classified as
 8136 1066 |    a = bogus
 1122 7998 |    b = real 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{verbatim}
%Kappa statistic                          0.7612
%Mean absolute error                      0.1483
%Root mean squared error                  0.3042
%Relative absolute error                 29.6645 %
%Root relative squared error             60.8425 %

Para este clasificador se halló un rendimiento comparable al Random Forest
entrenado sobre todo el conjunto de datos.
Esto muestra que al reducir el conjunto de \textit{features} que empleamos
para entrenar este tipo de clasificador el resultado mejora significativamente.
Por otro lado encontramos que la pérdida real de información al emplear 
esta transformación de los datos es muy pequeña.


\subsubsection{Random Forest}
Se entrenó un clasificador Random Forest con la submuestra extraída del análisis PCA.
Al igual que en los otros clasificadores se utiliza \textit{cross-correlation} en 10 hojas.
Se incluye a continuación la salida de pantalla de Weka.

\begin{verbatim}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
=== Run information ===

Scheme:weka.classifiers.trees.RandomForest -I 100 -K 0 -S 1
Relation:     AT: realbogus-weka.filters.unsupervised.attribute...
Instances:    18322
Attributes:   311
[list of attributes omitted]
Test mode:10-fold cross-validation

=== Classifier model (full training set) ===
Random forest of 100 trees, 
each constructed while considering 9 random features.

Out of bag error: 0.1733
Time taken to build model: 220.78 seconds

=== Stratified cross-validation ===

=== Summary ===
Correctly Classified Instances       15352               83.79   %
Incorrectly Classified Instances      2970               16.21   %
Total Number of Instances            18322     

=== Detailed Accuracy By Class ===
       TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
         0.848     0.172      0.833     0.848     0.84       0.919    bogus
         0.828     0.152      0.843     0.828     0.836      0.919    real
W. Avg.  0.838     0.162      0.838     0.838     0.838      0.919

=== Confusion Matrix ===
    a    b   <-- classified as
 7801 1401 |    a = bogus
 1569 7551 |    b = real
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{verbatim}
%Kappa statistic                          0.6758
%Mean absolute error                      0.3375
%Root mean squared error                  0.3711
%Relative absolute error                 67.4917 %
%Root relative squared error             74.2222 %

Podemos ver que para este clasificador el rendimiento se redujo considerablemente
en comparación con el Random Forest aplicado para la muestra completa.
Vemos que el \'area bajo la curva ROC obtenida es menor a el modelo de Random Forest
entrenado con el conjunto de features completo, y atribuimos esto a la reducción
considerable del 60\% de la cantidad de \textit{features} disponibles luego
de la transformación de los datos según el análisis PCA.\\
%
Hallamos interesante este resultado ya que es notable la reducción de rendimiento
con un conjunto más reducido de \textit{features}. Esto esta directamente relacionado
con las características de este clasificador, el cual trabaja entrenando sobre 
árboles con \textit{features} al azar, el cual necesita de muchas \textit{features} y 
apela a principios estadísticos relacionados con la Ley de los Grandes Números.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Resultados para la muestra seleccionada mediante \textbf{CfsSubsetEval}}
Esta muestra se conforma de 39 \textit{features} elegidos mediante el algoritmo \textbf{CfsSubsetEval} 
de Weka. 
Este conjunto de datos es mucho más pequeño que los anteriores, y por lo tanto los modelos entrenados con estos 
datos serán mucho mas simples.


\subsubsection{Naive Bayes}
Se entrenó un clasificador tipo Naive Bayes con la submuestra obtenida de seleccionar
nuestros mejores \textit{features} mediante el algoritmo \textbf{CfsSubsetEval}.
Se incluye a continuación parte de la salida del \textit{log} de resultados.
Se muestran además los resultados de la evaluación de errores mediante una
\textit{cross-correlation} en 10 hojas.
\begin{verbatim}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
=== Run information ===

Scheme:weka.classifiers.bayes.NaiveBayes 
Relation:     realbogus-weka.filters.unsupervised.attribute...
Instances:    18322
Attributes:   40
Test mode:10-fold cross-validation
Time taken to build model: 0.17 seconds

=== Stratified cross-validation ===

=== Summary ===
Correctly Classified Instances       11125               60.7194 %
Incorrectly Classified Instances      7197               39.2806 %
Total Number of Instances            18322     

=== Detailed Accuracy By Class ===
       TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
         0.964     0.753      0.564     0.964     0.711      0.818    bogus
         0.247     0.036      0.872     0.247     0.385      0.818    real
W. Avg.  0.607     0.396      0.717     0.607     0.549      0.818

=== Confusion Matrix ===
    a    b   <-- classified as
 8871  331 |    a = bogus
 6866 2254 |    b = real
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{verbatim}
%Kappa statistic                          0.2119
%Mean absolute error                      0.3923
%Root mean squared error                  0.6197
%Relative absolute error                 78.4703 %
%Root relative squared error            123.9314 %
Se obtuvo una relativa mejora, considerando las implementaciones de Naive Bayes
en la muestra completa y en la submuestra cortada por PCA, esto puede notarse
en la proporción de instancias clasificadas de forma correcta y de forma
errónea.
Notamos una leve mejoría del rendimiento si lo cuantificamos con el área bajo
la curva ROC, que suma 0.82 aproximadamente, contra un área de 0.75 y 0.65 en las
anteriores implementaciones.
Es de notar que Naive Bayes incrementa su rendimiento
de forma inversamente proporcional al número total de \textit{features} en este caso.


\subsubsection{Regresión Logística}
Se muestra los resultados de la implementación de Regresión Logística en la muestra
seleccionada.
Nuevamente se empleó una \textit{cross-correlation} en 10 hojas para determinar el rendimiento
del clasificador.
Se incluye a continación la salida de pantalla de resultados de Weka.
\begin{verbatim}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
=== Run information ===

Scheme:weka.classifiers.functions.Logistic -R 1.0E-8 -M -1
Relation:     realbogus-weka.filters.unsupervised.attribute...
Instances:    18322
Attributes:   40
Test mode:10-fold cross-validation

=== Classifier model (full training set) ===
Logistic Regression with ridge parameter of 1.0E-8

Time taken to build model: 2.07 seconds

=== Stratified cross-validation ===

=== Summary ===
Correctly Classified Instances       15972               87.1739 %
Incorrectly Classified Instances      2350               12.8261 %
Total Number of Instances            18322     

=== Detailed Accuracy By Class ===
      TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
        0.882     0.139      0.865     0.882     0.874      0.938    bogus
        0.861     0.118      0.879     0.861     0.87       0.938    real
W. Avg. 0.872     0.128      0.872     0.872     0.872      0.938

=== Confusion Matrix ===
    a    b   <-- classified as
 8117 1085 |    a = bogus
 1265 7855 |    b = real
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{verbatim}
%Kappa statistic                          0.7435
%Mean absolute error                      0.1939
%Root mean squared error                  0.3092
%Relative absolute error                 38.776  %
%Root relative squared error             61.8314 %
Para esta implementación de Regresión Logística notamos que los valores de área ROC y
de errores tipo I y II son similares a la implementación anterior.
Encontramos que el rendimiento para este clasificador es equivalente para ambas muestras 
reducidas, según el método de ésta muestra y segun una transformación en PCA.

\subsubsection{Random Forest}
Se entrenó un clasificador Random Forest con la submuestra extraída del análisis \textbf{CfsSubsetEval}, 
pero se utilizaron 300 árboles para realizar el entrenamiento. 
Al igual que en los otros clasificadores se utiliza \textit{cross-correlation} en 10 hojas.
Se incluye a continuación la salida de pantalla de Weka.

\begin{verbatim}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
=== Run information ===

Scheme:weka.classifiers.trees.RandomForest -I 100 -K 0 -S 1
Relation:     realbogus-weka.filters.unsupervised...
Instances:    18322
Attributes:   40
Test mode:10-fold cross-validation

=== Classifier model (full training set) ===
Random forest of 100 trees, 
each constructed while considering 6 random features.
Out of bag error: 0.1003

Time taken to build model: 28.51 seconds

=== Stratified cross-validation ===

=== Summary ===
Correctly Classified Instances       16540               90.274  %
Incorrectly Classified Instances      1782                9.726  %
Kappa statistic                          0.8055
Mean absolute error                      0.1814
Root mean squared error                  0.2767
Relative absolute error                 36.2754 %
Root relative squared error             55.3382 %
Total Number of Instances            18322     

=== Detailed Accuracy By Class ===
      TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
        0.914     0.108      0.895     0.914     0.904      0.963    bogus
        0.892     0.086      0.911     0.892     0.901      0.963    real
W. Avg. 0.903     0.097      0.903     0.903     0.903      0.963

=== Confusion Matrix ===
    a    b   <-- classified as
 8407  795 |    a = bogus
  987 8133 |    b = real
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{verbatim}
%Kappa statistic                          0.7435
%Mean absolute error                      0.1939
%Root mean squared error                  0.3092
%Relative absolute error                 38.776  %
%Root relative squared error             61.8314 %
Se encontró que este clasificador es que mejor rendimiento posee. 
Cabe destacar que el modelo utiliza el mismo número de árboles para un número de 
features 27 veces menor. Esta relación podría cambiar el rendimiento de un Random Forest 
con la muestra total, es decir si usáramos 2700 árboles para nuestras 1060 \textit{features}.
Sin embargo este tipo de implementaciones tomaría demasiado tiempo con el hardware
que se utilizó en este informe.\\
Aún así se da a notar que el clasificador es mejor que todos los implementados anteriormente
en todos los estimadores, llegando a mostrar un área ROC de $0.96$.
La matriz de confusión muestra simetría en los errores de tipos I y II para ambas clases.

\section{Conclusiones}
El trabajo se resume en los siguientes puntos:
\begin{itemize}
 \item Se simularon imágenes astronómicas con defectos observacionales típicos, 
y con inyecciones de objetos transitorios.
 \item Se aplicaron técnicas de Fotometría de diferencias de imágenes, y se identificaron
candidatos falsos producto de el ruido observacional.
 \item Se recolectaron los transitorios inyectados en las imágenes, y se 
plasmaron, ambos candidatos reales y candidatos falsos, en estampillas de $64 \times 64$ píxeles.
 \item Se almacenaron 18322 estampillas, 9120 \texttt{reals} y 9202 \texttt{bogus},
 y se computaron 1060 \textit{features} para cada estampilla.
 \item Se analizó la muestra de \textit{features} mediante dos técnicas de selección, generando
 dos muestras alternativas a la original.
 \item Se entrenaron tres tipos de clasificadores para cada muestra, a saber: Naive Bayes, Regresión Logística,
 y Random Forest.
 \item Se halló un rendimiento superior para el clasificador Random Forest entrenado con 39 \textit{features}
 seleccionadas mediante el algoritmo \textbf{CfsSubsetEval} de Weka, obteniendo un TPR de $0.9$ y un FPR menor al $0.1$.
\end{itemize}

%\begin{thebibliography}
\bibliographystyle{baaa}

\bibliography{biblio}

%\end{thebibliography}






\end{document}          
