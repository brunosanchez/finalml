#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------#
import os
import re
import shlex
import subprocess
import sys
from random import randint, random
from astropy.io import ascii
from pyraf.iraf import artdata, images, imutil, noao

#os.mkdir('./bogus')
Nb = 4455
images()
images.imutil()

images.imutil.imarith.unlearn()
noao()
noao.artdata()
noao.artdata.mknoise.unlearn()

artdata.gallist.unlearn()
artdata.gallist.gallist = 'listgx'
artdata.gallist.ngals=50
artdata.gallist.interactive = 'no'
artdata.gallist.xmin = 1.
artdata.gallist.ymin = 1.
artdata.gallist.xmax = 2048.
artdata.gallist.ymax = 2048.


artdata.starlist.unlearn()
artdata.starlist.starlist = 'listst'
artdata.starlist.nstars = 100
artdata.starlist.interactive = 'no'
artdata.starlist.xmin = 1.
artdata.starlist.ymin = 1.
artdata.starlist.xmax = 2048.
artdata.starlist.ymax = 2048.

for l in xrange(3):
    if os.path.isfile('listgx'): os.remove('listgx')
    if os.path.isfile('listst'): os.remove('listst')
    artdata.gallist(gallist='listgx', ngals=80,lseed=randint(0,300), sseed=randint(0,300))
    artdata.starlist(starlist='listst', nstars=150, lseed=randint(0,300), sseed=randint(0,300))

    #listas de objetos generadas
    #for i in xrange(10):
    #creacion de una imagen de 512 por 512
    artdata.mkobjects.unlearn()
    artdata.mkobjects.comment='no'
    artdata.mkobjects.exptime=10.
    artdata.mkobjects.nlines = 512
    artdata.mkobjects.ncols = 512

    #creacion de un frame con offset aleatorio
    #para usar como maestra
    for m in xrange(100):
        xoff, yoff = -random()*(2048-512), -random()*(2048-512)
        print xoff,yoff
        artdata.mkobjects(input='fk.fits', objects='listst', xoff=xoff, yoff=yoff)
        artdata.mkobjects(input='fk.fits', output='fk.fits', objects='listgx', xoff=xoff, yoff=yoff)
        #agrego ruido a la imagen y la convierto en maestra
        artdata.mknoise.seed= randint(0,500)
        artdata.mknoise(input='fk.fits',output='master.fits',poisson='yes',ncosray=3)
        os.remove('fk.fits')

        #reutilizo la maestra varias veces, para obtener muchas realizaciones de ruido
        for o in xrange(2):
            xoff, yoff = xoff+(random()-0.5)*2., yoff+(random()-0.5)*2.
            print xoff, yoff
            artdata.mkobjects(input='fk.fits', objects='listst', xoff=xoff, yoff=yoff)
            artdata.mkobjects(input='fk.fits', output='fk.fits', objects='listgx', xoff=xoff, yoff=yoff)
            #agrego ruido a la imagen y la convierto en nueva imagen
            artdata.mknoise.seed= randint(0,200)
            artdata.mknoise(input='fk.fits', output='observ.fits', poisson='yes',ncosray=2)
            os.remove('fk.fits')
            images.imutil.imarith(operand1='master.fits',op='-', operand2='observ.fits',\
                                  result = 'resta.fits')
            command = 'sex resta.fits -c default.sex.differential -CATALOG_NAME \
                diff.cat -PARAMETERS_NAME diff.param'
            arg=shlex.split(command)
            #calling them
            #----------------------------------------------------------------
            os.system(command)#subprocess.call(arg)
            #----------------------------------------------------------------
            ima = 'resta.fits'
            cat = 'diff.cat'
            tab = ascii.read(table=cat, format='sextractor')
            for i in xrange(len(tab)):
                row = tab[i]
                cmd = "swarp -c default.swarp -VERBOSE_TYPE QUIET -RESAMPLE N -IMAGE_SIZE 64\
                    -CENTER_TYPE MANUAL -CENTER {}".format(str(row['X_IMAGE']))+\
                    ",{}".format(str(row['Y_IMAGE'])+ " " + ima)
                #print cmd
                #args = shlex.split(cmd)
                #subprocess.call(args)
                os.system(cmd)
                cmd2= "stiff -c default.stiff -VERBOSE_TYPE QUIET coadd.fits \
                    -OUTFILE_NAME ./bogus/obj_{}.tif".format(str(Nb).zfill(3))
                #print cmd2
                #arg2=shlex.split(cmd2)
                #subprocess.call(arg2)
                os.system(cmd2)
                Nb+=1
            #----------------------------------------------------------------
            if os.path.isfile('observ.fits'): os.remove('observ.fits')
            if os.path.isfile('resta.fits'): os.remove('resta.fits')
        if os.path.isfile('master.fits'): os.remove('master.fits')







