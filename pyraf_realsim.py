#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------#
import os
import re
import shlex
import subprocess
import sys
from random import randint, random
from astropy.io import ascii
from pyraf.iraf import artdata, images, imutil, noao

os.mkdir('./real')
Nr = 1
images()
images.imutil()

images.imutil.imarith.unlearn()
noao()
noao.artdata()
noao.artdata.mknoise.unlearn()

#create a star list and a gx list for background sky
artdata.gallist.unlearn()
artdata.gallist.gallist = 'listgx'
artdata.gallist.ngals=20
artdata.gallist.interactive = 'no'
artdata.gallist.xmin = 1.
artdata.gallist.ymin = 1.
artdata.gallist.xmax = 1024.
artdata.gallist.ymax = 1024.


artdata.starlist.unlearn()
artdata.starlist.starlist = 'listst'
artdata.starlist.nstars = 70
artdata.starlist.interactive = 'no'
artdata.starlist.xmin = 1.
artdata.starlist.ymin = 1.
artdata.starlist.xmax = 1024.
artdata.starlist.ymax = 1024.
for l in xrange(3):
    if Nr < 6000:
        artdata.gallist(gallist='listgx', ngals=80,lseed=randint(0,300), sseed=randint(0,300))
        artdata.starlist(starlist='listst', nstars=150,lseed=randint(0,300), sseed=randint(0,300))

    # generate the master. and adding noise
        artdata.mkobjects.unlearn()
        artdata.mkobjects.comment='no'
        artdata.mkobjects.exptime=5.
        artdata.mkobjects.nlines = 1024
        artdata.mkobjects.ncols = 1024

        #creacion de un frame sin offset
        #para usar como maestra
        for m in xrange(100):
            if Nr < 6000:
                if os.path.isfile('master.fits'): os.remove('master.fits')
                #xoff, yoff = -random()*(2048-512), -random()*(2048-512)
                #print xoff,yoff
                artdata.mkobjects(input='fk.fits', objects='listst')#, xoff=xoff, yoff=yoff)
                artdata.mkobjects(input='fk.fits', output='fk.fits', objects='listgx')#, xoff=xoff, yoff=yoff)
                #agrego ruido a la imagen y la convierto en maestra
                artdata.mknoise.seed= randint(0,200)
                artdata.mknoise(input='fk.fits',output='master.fits',poisson='yes',ncosray=2)
                os.remove('fk.fits')
        # generate the transient background image, and the transient list
                for o in xrange(2):
                    if Nr < 6000:
                        #xoff, yoff = xoff+(random()-0.5)*2., yoff+(random()-0.5)*2.
                        #print xoff, yoff
                        artdata.mkobjects(input='fk.fits', objects='listst')#, xoff=xoff, yoff=yoff)
                        artdata.mkobjects(input='fk.fits', output='fk.fits', objects='listgx')#, xoff=xoff, yoff=yoff)
                        artdata.starlist(starlist='transient_list', nstars=5,lseed=randint(0,300), sseed=randint(0,300))
                        artdata.mkobjects(input='fk.fits', output='fk.fits', objects='transient_list')#, xoff=xoff, yoff=yoff)
                        #agrego ruido a la imagen y la convierto en nueva imagen observ
                        artdata.mknoise.seed= randint(0,700)
                        artdata.mknoise(input='fk.fits', output='observ.fits', poisson='yes',ncosray=2)
                        os.remove('fk.fits')
            #difference image
                        if os.path.isfile('resta.fits'): os.remove('resta.fits')
                        images.imutil.imarith(operand1='observ.fits',op='-', operand2='master.fits',\
                                              result = 'resta.fits')
            #patching and stiffing the image where the transient stars are
                        os.remove('observ.fits')
                        f = open('transient_list')
                        for line in f.xreadlines():
                            if line[0]!='#':
                                x = line.split()[0]
                                y = line.split()[1]
                                cmd = "swarp -c default.swarp -VERBOSE_TYPE QUIET -RESAMPLE N -IMAGE_SIZE 64\
                                -CENTER_TYPE MANUAL -CENTER {}".format(str(x))+\
                                ",{}".format(str(y)+ " resta.fits")
                                print cmd
                                os.system(cmd)
                                cmd2= "stiff -c default.stiff -VERBOSE_TYPE QUIET coadd.fits \
                                -OUTFILE_NAME ./real/robj_{}.tif".format(str(Nr).zfill(4))
                                print cmd2
                                os.system(cmd2)
                                Nr+=1
                            #----------------------------------------------------------------

if os.path.isfile('master.fits'): os.remove('master.fits')
if os.path.isfile('observ.fits'): os.remove('observ.fits')
if os.path.isfile('resta.fits'): os.remove('resta.fits')
for l in xrange(3):
    if os.path.isfile('listst'): os.remove('listst')
    if os.path.isfile('listgx'): os.remove('listgx')
    artdata.gallist(gallist='listgx', ngals=80,lseed=randint(0,300), sseed=randint(0,300))
    artdata.starlist(starlist='listst', nstars=150,lseed=randint(0,300), sseed=randint(0,300))

# generate the master. and adding noise
    artdata.mkobjects.unlearn()
    artdata.mkobjects.comment='no'
    artdata.mkobjects.exptime=5.
    artdata.mkobjects.nlines = 1024
    artdata.mkobjects.ncols = 1024

    #creacion de un frame sin offset
    #para usar como maestra
    for m in xrange(100):
        artdata.mkobjects(input='fk.fits', objects='listst')#, xoff=xoff, yoff=yoff)
        artdata.mkobjects(input='fk.fits', output='fk.fits', objects='listgx')#, xoff=xoff, yoff=yoff)
        #agrego ruido a la imagen y la convierto en maestra
        artdata.mknoise.seed= randint(0,200)
        artdata.mknoise(input='fk.fits',output='master.fits',poisson='yes',ncosray=2)
        os.remove('fk.fits')
# generate the transient background image, and the transient list
        for o in xrange(2):
            xoff, yoff = (random()-0.5)*2., (random()-0.5)*2.
            print xoff, yoff
            artdata.mkobjects(input='fk.fits', objects='listst', xoff=xoff, yoff=yoff)
            artdata.mkobjects(input='fk.fits', output='fk.fits', objects='listgx', xoff=xoff, yoff=yoff)
            if os.path.isfile('transient_list'): os.remove('transient_list')
            artdata.starlist(starlist='transient_list', nstars=5, lseed=randint(0,300), sseed=randint(0,300))
            artdata.mkobjects(input='fk.fits', output='fk.fits', objects='transient_list')#, xoff=xoff, yoff=yoff)
            #agrego ruido a la imagen y la convierto en nueva imagen observ
            artdata.mknoise.seed= randint(0,700)
            artdata.mknoise(input='fk.fits', output='observ.fits', poisson='yes',ncosray=2)
            os.remove('fk.fits')
#difference image
            images.imutil.imarith(operand1='observ.fits',op='-', operand2='master.fits',\
                                  result = 'resta.fits')
#patching and stiffing the image where the transient stars are
            os.remove('observ.fits')
            f = open('transient_list')
            for line in f.xreadlines():
                if line[0]!='#':
                    x = line.split()[0]
                    y = line.split()[1]
                    cmd = "swarp -c default.swarp -VERBOSE_TYPE QUIET -RESAMPLE N -IMAGE_SIZE 64\
                    -CENTER_TYPE MANUAL -CENTER {}".format(str(x))+\
                    ",{}".format(str(y)+ " resta.fits")
                    print cmd
                    os.system(cmd)
                    cmd2= "stiff -c default.stiff -VERBOSE_TYPE QUIET coadd.fits \
                    -OUTFILE_NAME ./real/robj_{}.tif".format(str(Nr).zfill(4))
                    print cmd2
                    os.system(cmd2)
                    Nr+=1
                #----------------------------------------------------------------
            f.close()
            os.remove('resta.fits')
        os.remove('master.fits')


